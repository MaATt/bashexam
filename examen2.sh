#!/bin/bash


function validate_url(){
  if [[ `wget -S --spider $1  2>&1 | grep 'HTTP/1.1 200 OK'` ]]; then
    return 0
  else
    return 1
  fi
}


echo "Quitter et télécharger curl si ce n'est pas fait (apt install curl)"
echo "Taper l'url à télécharger"
read URL
echo "Dans quel fichier voulez-vous télécharger cet url"
read NOM_FICHIER

if validate_url $URL; then    
    echo "url ok"
    OUTPUT="$NOM_FICHIER.txt"
    > $NOM_FICHIER
    curl -L $URL > $NOM_FICHIER

  else
    echo "url est non valide"
    exit 1
fi





